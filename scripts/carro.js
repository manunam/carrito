'use strict';

(() => {
    let obtenerImporte = (textoImporte) => Number(textoImporte.split(' ')[0]);

    let articulo = document.querySelector('.item:first-child');

    let copia = articulo.cloneNode(true);

    copia.id = 'c' + articulo.id;
    copia.querySelector('.stock').style.display = 'none';
    copia.style.cursor = 'default';
    copia.childNodes.forEach(hijo => {
        if (hijo.nodeType === Node.ELEMENT_NODE) 
            hijo.style.cursor = 'default'
    });

    let enlace = document.createElement('a');
    enlace.href = '';
    enlace.className = 'delete';

    copia.insertBefore(enlace, copia.firstChild);

    let cartItems = document.getElementById('cart_items');
    cartItems.insertBefore(copia, cartItems.firstChild);

    let stock = articulo.querySelector('.stock');

    let arrStock = stock.innerText.split(' ');
    let valorStock = Number(arrStock[1]) - 1; 
    stock.innerText = arrStock[0] + ' ' + valorStock;

    if (valorStock === 0)
        stock.classList.add('agotado');

    let citem = document.getElementById('citem');
    citem.value = Number(citem.value)+1;

    let price = articulo.querySelector('.price');
    let precio = obtenerImporte(price.innerText);
    let cprice = document.getElementById('cprice');
    cprice.value = (obtenerImporte(cprice.value)+precio) + ' €';
let idTime = setTimeout (function () {
    console.log(new Date().toString());
}, 5000);

})();